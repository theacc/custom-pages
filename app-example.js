define(function() {
  function QelloClient(networkId, deviceName, subassetId) {
    this.apiBase = "https://stadium-syndication-prod.sidearmsports.com";
    this.networkId = networkId;
    this.deviceName = deviceName;

    this.token = this.getToken().then(function(resp) {
      return resp.data.token;
    });

    var sports = $.Deferred();
    this.getSports = function() {
      return sports.promise();
    };
    var schools = $.Deferred();
    this.getSchools = function() {
      return schools.promise();
    };
    var categories = $.Deferred();
    this.getCategories = function() {
      return categories.promise();
    };

    this.subasset = this.token.then(
      function(token) {
        return this.apiCall("GET", "/subassets/", {
          token: token,
          id: subassetId
        }).then(
          function(resp) {
            sports.resolve(
              resp.data.assets.tags.filter(function(tag) {
                return tag.parent === "Sport";
              })
            );
            schools.resolve(
              resp.data.assets.tags.filter(function(tag) {
                return tag.parent === "School";
              })
            );
            categories.resolve(
              resp.data.assets.tags.filter(function(tag) {
                return tag.parent === "Series Title";
              })
            );
            return resp;
          }.bind(this)
        );
      }.bind(this)
    );
  }

  QelloClient.prototype = {
    dataToQueryString: function(data) {
      if (!data) return "";

      var parts = [];
      Object.keys(data).forEach(function(key) {
        if ($.isArray(data[key])) {
          data[key].forEach(function(value) {
            parts.push({
              name: key,
              value: value
            });
          });
        } else {
          parts.push({
            name: key,
            value: data[key]
          });
        }
      });

      return parts
        .filter(function(part) {
          return part.value != null;
        })
        .map(function(p) {
          return p.name + "=" + p.value;
        })
        .join("&");
    },
    apiUrl: function(method, path, data) {
      return this.apiBase + path + "?" + this.dataToQueryString(data);
    },
    apiCall: function(method, path, data) {
      return $.ajax({
        url: this.apiBase + path + "?" + this.dataToQueryString(data),
        method: method
      });
    },
    getToken: function() {
      if (!window.localStorage.qello_device_id) {
        window.localStorage.qello_device_id =
          Date.now() + "." + (Math.floor(Math.random() * 1000000) + 1000000);
      }
      var device_id = window.localStorage.qello_device_id;
      var app_version = 1;

      return this.apiCall("GET", "/token/", {
        "device_data[device_name]": this.deviceName,
        "device_data[device_id]": device_id,
        "device_data[app_version]": app_version
      });
    },
    getLiveStreams: function(limit, tags, id) {
      tags = [this.networkId].concat(tags || []).filter(function(tag) {
        return !!tag;
      });

      return this.token.then(
        function(token) {
          return this.apiCall("GET", "/liveevents/", {
            token: token,
            "tag[]": tags,
            limit: limit,
            tag_search_type: "and",
            order: "meta.start_time",
            "meta.end_time_gte_date": Math.floor(Date.now() / 1000),
            order_type: "asc",
            id: id
          }).then(function(resp) {
            return resp.data.liveevents.map(function(asset) {
              return {
                type: "Live",
                id: asset.id,
                adConfigId: asset.ad_config_id,
                isLive: asset.is_live,
                dateTime: new Date(asset.meta.start_time * 1000),
                title: asset.meta.name,
                sport: asset.tags
                  .filter(function(tag) {
                    return tag.parent === "Sport";
                  })
                  .map(function(tag) {
                    return tag.name;
                  })
                  .join(", "),
                school: asset.tags
                  .filter(function(tag) {
                    return tag.parent === "School";
                  })
                  .map(function(tag) {
                    return tag.name;
                  })
                  .join(", "),
                image: (asset.images || {}).image,
                meta: asset.meta
              };
            });
          });
        }.bind(this)
      );
    },
    getContent: function(limit, tags, id) {
      tags = [this.networkId].concat(tags || []).filter(function(tag) {
        return !!tag;
      });

      return this.token.then(
        function(token) {
          return this.apiCall("GET", "/content/", {
            token: token,
            "tag[]": tags,
            limit: limit,
            tag_search_type: "and",
            order: "created",
            order_type: "desc",
            id: id
          }).then(function(resp) {
            return resp.data.assets.map(function(asset) {
              return {
                type: "VOD",
                id: asset.id,
                adConfigId: asset.ad_config_id,
                dateTime:new Date(
                  (asset.date_publish || asset.created) * 1000
                ),
                duration: asset.duration || null,
                title: asset.meta.name,
                sport: asset.tags
                  .filter(function(tag) {
                    return tag.parent === "Sport";
                  })
                  .map(function(tag) {
                    return tag.name;
                  })
                  .join(", "),
                school: asset.tags
                  .filter(function(tag) {
                    return tag.parent === "School";
                  })
                  .map(function(tag) {
                    return tag.name;
                  })
                  .join(", "),
                image: (asset.images || {}).image
              };
            });
          });
        }.bind(this)
      );
    },
    getSports: function() {
      return this.token.then(
        function(token) {
          return this.apiCall("GET", "/tags/", {
            token: token,
            name: "Sport",
            level: 0
          }).then(function(resp) {
            return resp.data.tags[0].children;
          });
        }.bind(this)
      );
    },
    getSchools: function() {
      return this.token.then(
        function(token) {
          return this.apiCall("GET", "/tags/", {
            token: token,
            name: "School",
            level: 0
          }).then(function(resp) {
            return resp.data.tags[0].children;
          });
        }.bind(this)
      );
    },
    getStreamUrl: function(asset) {
      return this.token.then(
        function(token) {
          return this.apiUrl("GET", "/broker/", {
            token: token,
            asset_id: asset.id,
            codec: "HLS"
          });
        }.bind(this)
      );
    }
  };
  return QelloClient;
});