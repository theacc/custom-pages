# [theacc.com custom pages](//theacc.com)

Version: 1.0.0

## Author:

Tony Lisanti ( [tonylisanti.com](http://tonylisanti.com) )

## Summary

A repository for custom pages on theacc.com

## Required Applications

* [git](https://git-scm.com/downloads)

## Optional Applications

### Editors

* [sublime](https://www.sublimetext.com/3)
* [atom](https://atom.io/)
* [visual studio code](https://code.visualstudio.com/download)

### Source Control

* [sourcetree](https://www.sourcetreeapp.com/)
