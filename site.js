// working towards react/vue

$(window).load(function () {
	
	const bcAccountId = '6050660109001';

	let scrollToOffset = $('.main-header__navigation').height() + $('.main-header__filter').height();

	var bc = {
		token: function() {
		  return $.getJSON('https://static.theacc.com/custompages/bc-token.aspx').then(function(data) {
		    //return data.data.token;
				return data.access_token;
		  });
		},
	  video: function(id) {
	    return this.token().then(function(token) {
	    	if(id !== undefined) {
			    return $.getJSON('https://cms.api.brightcove.com/v1/accounts/'+bcAccountId+'/videos/'+id+'?access_token='+token).then(function(data) {
			      return data;
			    });
		  	} else {
			    return $.getJSON('https://cms.api.brightcove.com/v1/accounts/'+bcAccountId+'/videos?access_token='+token+'&limit=1&sort=created_at&q=%2Bnetwork:ACCDN%20%2Bschool:Miami').then(function(data) {
			      return data;
			    });
		  	}
	    });
	  },
		videos : function(query) {
			return this.token().then(function(token) {
				//return $.getJSON('https://cms.api.brightcove.com/v1/accounts/'+bcAccountId+'/videos?access_token='+token+'&limit=6&sort=created_at&q=%2Bseries_title:%2BACC%20%2bCondensed%20%2bGame&offset='+offset).then(function(data) {
				return $.getJSON('https://cms.api.brightcove.com/v1/accounts/'+bcAccountId+'/videos?access_token='+token+query).then(function(data) {
					return data;
				});
			});
		}

	};

	var sidearm = {
		stories : function(query) {
			//return $.getJSON('/api/stories_xml?format=json&count=4&path='+saPath+'&page='+page).then(function(data) {
			return $.getJSON('/api/stories_xml?format=json'+query).then(function(data) {
				return data.stories;
			});
		},
		archives : function(query) {
			//return $.getJSON('/api/stories_xml?format=json&count=4&path='+saPath+'&page='+page).then(function(data) {
			return $.getJSON('/api/tournaments?id='+query).then(function(data) {
				return data;
			});
		},
		championship : function(query) {
			//return $.getJSON('/api/stories_xml?format=json&count=4&path='+saPath+'&page='+page).then(function(data) {
			return $.getJSON('/api/tournaments?id='+query).then(function(data) {
				return data;
			});
		},
		standings: function(query) {
			//return $.getJSON('http://theacc.com/api/standings?view=simple&detail=short&path='+saPath).then(function(data) {
			return $.getJSON('/api/standings?view=simple&detail=short'+query).then(function(data) {
				return data;
			});
		}
	};

	// common
	$(window).resize(function(){
	  equalHeight('.feed .feed-item');
	});

	// sliders

	/* custom componets
	**************************************************/

	// videos

	if($('#component-videos').length) {
		let query = {};
		
		if(content.videos != 'undefined') {
			addObjectElement(query, content.videos );
		}

		if(typeof content.videos.limit == 'undefined') {
			addObjectElement(query, { "limit" : "6" } );
		}

		if(typeof content.videos.offset == 'undefined') {
			addObjectElement(query, { "offset" : "0" } );
		}

		if(typeof content.videos.sort == 'undefined') {	
			addObjectElement(query, { "sort" : "-published_at" } );
		}

		if(typeof content.videos.customFields != 'undefined') {	
			addObjectElement(query, { "customFields" : content.videos.customFields } );
		}		

		$('#component-videos').append(componentVideos());
		$("#component-videos .feed-load-more").attr('data-query', JSON.stringify(query));
		
		query = buildBCQuery(query);

		bc.videos(query).then(function(data) {
			let html = processVideoFeed(data);
			$('#component-videos .feed-list')
				.append(html)
				.append(function() { 
					$(this).find('.feed-item').click(function(){
					  var $videoId = $(this).attr('id');
						bc.video($videoId).done(function(data) {
							var $src = '//players.brightcove.net/6050660109001/LgU98Y522_default/index.html?&adConfigId=&autoplay=true&videoId='+data.id;
							let date = moment(data.created_at).format("ddd, DD MMM YYYY");

						  $("#component-hero iframe").attr("src", $src);
						  $('#component-hero #hero-title').html(data.name);
						  $('#component-hero #hero-date').html(date);
				      $('html,body').animate({
				        scrollTop: $('#component-hero').offset().top - scrollToOffset
				      }, 250);
						});
					});
				});
		})
		.then(function(){
			equalHeight('#component-videos .feed .feed-list .feed-item');
		});
	}

	$('#component-videos .feed-load-more').click(function(){
		let query = JSON.parse($("#component-videos .feed-load-more").attr('data-query'));
		let offset = parseInt(query.offset) + parseInt(query.limit);

		delete query.offset;
		addObjectElement(query, { 'offset' : offset });

		$("#component-videos .feed-load-more").attr('data-query', JSON.stringify(query));
		
		query = buildBCQuery(query);

		bc.videos(query).done(function(data) {
			let html = processVideoFeed(data);
			$('#component-videos .feed-list').append(html).append(function() { 
				$(this).find('.feed-item').click(function(){
				  let $videoId = $(this).attr('id');
					bc.video($videoId).done(function(data) {
						let $src = '//players.brightcove.net/6050660109001/LgU98Y522_default/index.html?&adConfigId=&autoplay=true&videoId='+data.id;
						let date = moment(data.created_at).format("ddd, DD MMM YYYY");

					  $("#component-hero iframe").attr("src", $src);
					  $('#component-hero #hero-title').html(data.name);
					  $('#component-hero #hero-date').html(date);
			      $('html,body').animate({
			        scrollTop: $('#component-hero').offset().top - scrollToOffset
			      }, 250);
					});
				});
			});
		});
	});

	$( '#all-access-sport-select, #all-access-school-select' ).change(function() {
		let id = $(this).attr('id');
		let value = $(this).val();
		let query = JSON.parse($("#component-videos .feed-load-more").attr('data-query'));

		delete query.offset;
		addObjectElement(query, { "offset" : 0 });

		if(id == 'all-access-sport-select') {
			if(typeof query.customFields.sport == 'undefined' && value != 0) {
				addObjectElement(query.customFields, { "sport" : value });
			} else if (value != 0) {
				delete query.customFields.sport;
				addObjectElement(query.customFields, { "sport" : value });
			} else {
				delete query.customFields.sport;
			}
		} else {
			if(typeof query.customFields.schools == 'undefined' && value != 0) {
				addObjectElement(query.customFields, { "schools" : [value] });
			} else if (value != 0) {
				delete query.customFields.schools;
				addObjectElement(query.customFields, { "schools" : [value] });
			} else {
				delete query.customFields.schools;
			}
		}

		$("#component-videos .feed-load-more").attr('data-query', JSON.stringify(query));

		query = buildBCQuery(query);
		
		$('#component-videos .feed-list').empty();

		bc.videos(query).done(function(data) {
			let html = processVideoFeed(data);
			$('#component-videos .feed-list').append(html).append(function() { 
				$(this).find('.feed-item').click(function(){
				  let $videoId = $(this).attr('id');
					bc.video($videoId).done(function(data) {
						let $src = '//players.brightcove.net/6050660109001/LgU98Y522_default/index.html?&adConfigId=&autoplay=true&videoId='+data.id;
						let date = moment(data.created_at).format("ddd, DD MMM YYYY");

					  $("#component-hero iframe").attr("src", $src);
					  $('#component-hero #hero-title').html(data.name);
					  $('#component-hero #hero-date').html(date);
			      $('html,body').animate({
			        scrollTop: $('#component-hero').offset().top - scrollToOffset
			      }, 250);
					});
				});
			});
		});
	});

	// stories

	if($('#component-stories').length) {
		let query = {};

		if(content.stores != 'undefined') {
			addObjectElement(query, content.stories);
		}

		if(typeof content.stories.count == 'undefined') {
			addObjectElement(query, { "count" : "6" } );
		}

		if(typeof content.stories.page == 'undefined') {
			addObjectElement(query, { "page" : "1" } );
		}

		if(typeof content.stories.sort == 'undefined') {	
			addObjectElement(query, { "sort" : "published_at" } );
		}

		if(typeof content.stories.path != 'undefined') {	
			addObjectElement(query, { "path" : content.stories.path } );
		}

		$('#component-stories').append(componentStories());
		$("#component-stories .feed-load-more").attr('data-query', JSON.stringify(query));

		query = buildSAQuery(query);

		sidearm.stories(query).then(function(data) {
			let html = processStories(data);
			$('#component-stories .feed-list').append(html);
		})
		.then(function(){
			equalHeight('#component-stories .feed .feed-list .feed-item');
		});
	}

	$('#component-stories .feed-load-more').click(function(){
		let query = JSON.parse($("#component-stories .feed-load-more").attr('data-query'));
		let page = parseInt(query.page) + 1;

		delete query.page;
		addObjectElement(query, { 'page' : page });

		$("#component-stories .feed-load-more").attr('data-query', JSON.stringify(query));

		query = buildSAQuery(query);

		sidearm.stories(query).done(function(data) {
			let html = processStories(data);
			$('#component-stories .feed-list').append(html);
		});
	});

	// promos

	if($('#component-promos').length) {
		let html = processPromos(content.promos);
		$('#component-promos').append(html);
		$('#component-promos .slick').slick({
		  slidesToShow: 4,
		  slidesToScroll: 1,
		  autoplay: true,
		  autoplaySpeed: 3500,
			dots : false,
			arrows : false,
			rows : 0,
			//lazyLoad: 'ondemand',
		  responsive: [
		    {
		      breakpoint: 576,
		      settings: {
		        slidesToShow: 1,
		      }
		    },
		    {
		      breakpoint: 768,
		      settings: {
		        slidesToShow: 2,
		      }
		    },
		    {
		      breakpoint: 992,
		      settings: {
		        slidesToShow: 3,
		      }
		    },
		    {
		      breakpoint: 1200,
		      settings: {
		        slidesToShow: 4,
		      }
		    }
		  ]
		});
	}

	// hero

	if($('#component-hero').length) {
		let query = {};

		if(content.videos) {
			addObjectElement(query, { "limit" : "1" } );
			addObjectElement(query, { "sort" : "-published_at" } );
			if(typeof content.videos.customFields != 'undefined') {	
				addObjectElement(query, { "customFields" : content.videos.customFields } );
			}
			query = buildBCQuery(query);

			bc.videos(query).done(function(videoData) {
				let date = moment(videoData['0'].created_at).format("ddd, DD MMM YYYY");
				let embed = '<iframe src="//players.brightcove.net/6050660109001/default_default/index.html?adConfigId=&autoplay=true&videoId='+videoData['0'].id+'" allowfullscreen="" frameborder="0" scrolling="no" mozallowfullscreen="" webkitallowfullscreen=""></iframe>';
				let html = processHero(embed, videoData['0'].name, date);
				$('#component-hero').append(html);
			});

		} else if(content.stories) {
			addObjectElement(query, { "count" : "1" } );
			addObjectElement(query, { "page" : "1" } );
			addObjectElement(query, { "sort" : "published_at" } );
			if(typeof content.stories.path != 'undefined') {	
				addObjectElement(query, { "path" : content.stories.path } );
			}

			query = buildSAQuery(query);

			sidearm.stories(query).done(function(storyData) {
				let embed = '<img src="'+storyData['0'].enclosure.url+'" />';
				let date = storyData['0'].pubDate;
				let substrEnd = date.length - 12;
        date = date.substr(0, substrEnd);
				let html = processHero(embed, storyData['0'].title, date, storyData['0'].link);
				$('#component-hero').append(html);
			});
		} else {
			let embed = '<img src="/images/2017/9/6/POW_LogoImage.png" />';
      let date = moment().format('ddd, DD MMM YYYY');
      let html = processHero(embed, 'Title not found...', date, '#');
			$('#component-hero').append(html);
		}
	}

	// featured links

	if($('#component-featured-links').length) {
		let html = processFeaturedLinks(content.featuredLinks);
		$('#component-featured-links').append(html);
	}

	// twitter

	if($('#component-social-twitter').length) {
		let html = processSocialTwitter(content.social.twitter);
		$('#component-social-twitter').append(html);
	}

	// weather

	if($('#component-weather').length && typeof content.weather != 'undefined') {
		let html = processWeather(content.weather);
		$('#component-weather').append(html);	
	}

	// standings

	if($('#component-standings').length && typeof content.championship != 'undefined') {
		let saPath = $("#component-standings").attr('data-path');

		sidearm.standings('&path='+saPath).done(function(data) {
		//sidearm.standings('&path=football').done(function(data) {
			let html = processStandings(data);
			$('#component-standings').html(html);
		});	
	}

	// championship page header

	if($('#component-page-header').length && typeof content.championship != 'undefined') {
		$('.article-wrapper .article-headline, .article-wrapper .article-image, .article-wrapper .article-subheadline, .article-wrapper .article-byline').hide();
		sidearm.championship(content.championship.id).done(function(data) {
			addObjectElement(content.championship, { 'sa' : data.data });
			processChampionshp(content);
		});
	}

	//functions

	//function buildBCQuery({bcLimit, bcSort, bcOffset, bcCustomFields}={}) {
	function buildBCQuery(bcQuery) {
		let query = '';

		query = typeof bcQuery.limit != 'undefined' ? query + '&limit=' + bcQuery.limit : query + '&limit=6';
		query = typeof bcQuery.offset != 'undefined' ? query + '&offset=' + bcQuery.offset : query + '&offset=0';
		query = typeof bcQuery.sort != 'undefined' ? query + '&sort=' + bcQuery.sort : query + '&sort=-published_at';
		query = query + '&q=';

		for (let key in bcQuery.customFields) {
    	if (bcQuery.customFields.hasOwnProperty(key)) {
				let customFieldLabel = key.toLowerCase();
				let arrSchool = '';

				customFieldLabel = customFieldLabel.replace(/ /g, '_');
				customFieldLabel = customFieldLabel + ':';

				if(key == 'schools' || key == 'school') {
					let schools = '';

					bcQuery.customFields.schools.forEach(function(content, index) {
						schools += content.replace(/ /g, '%20');
						if(index + 1 != bcQuery.customFields.schools.length) {
							schools += ',';
						}
					});
					query += '%2Bschool:' + schools + '%20';
					query += 'schools:' + schools + '%20';
					if(bcQuery.customFields.schools.find(function(value){ return value.search(/tech/i) > 0 }) == undefined) {
						query += '-school:Tech%20-school:tech%20';
						query += '-schools:Tech%20-schools:tech%20';
					} else {
						query += '%2Bschool:Tech%20';
					}
				} else {
					customFieldValue = '"' + bcQuery.customFields[key].replace(/ /g, '%20') + '"%20';
					if(key == 'sport') {
						if(bcQuery.customFields[key].search(/women's/i) != -1) {
							query += '-sport:"Men%27s"%20-sport:"Men%27s"%20';
						} else if(bcQuery.customFields[key].search(/men's/i) != -1) {
							query += '-sport:"Women%27s"%20-sport:"women%27s"%20';
						}
					} 
					query += '%2B' + customFieldLabel + customFieldValue;
				}
    	}
		}

		return query;
	}

	/*
	function buildBCQuery(bcQuery) {
		let query = '';

		query = typeof bcQuery.limit != 'undefined' ? query + '&limit=' + bcQuery.limit : query + '&limit=6';
		query = typeof bcQuery.offset != 'undefined' ? query + '&offset=' + bcQuery.offset : query + '&offset=0';
		query = typeof bcQuery.sort != 'undefined' ? query + '&sort=' + bcQuery.sort : query + '&sort=-published_at';
		query = query + '&q=';

		for (let key in bcQuery.customFields) {
    	if (bcQuery.customFields.hasOwnProperty(key)) {
				let customFieldLabel = key.toLowerCase();
				customFieldLabel = customFieldLabel.replace(/ /g, '_');
				customFieldLabel = customFieldLabel + ':';

				let customFieldValue = bcQuery.customFields[key];
				
				if (customFieldValue.indexOf(' ') === -1) {
					customFieldValue = customFieldValue + ' ';
				}	else {
					customFieldValue = '"' + customFieldValue.replace(/ /g, ' ') + '" ';	
				}

				if (customFieldLabel == 'school:') {
						console.log(customFieldValue);
					if (customFieldValue == 'Virginia ') {
						query = query + '+' + customFieldValue + '-Tech ';
					} else if (customFieldValue == '"North Carolina" ') {
						query = query + '+' + customFieldValue + '-State ';
					} else {
						query = query + '+' + customFieldValue;
					}
				} else {
					query = query + '+' + customFieldLabel + customFieldValue;
				}
    	}
		}
			console.log(query);
			console.log(encodeURI(query));

		//return encodeURIComponent(query);
		return encodeURI(query);
		//return query;
	}
	*/

	function componentVideos(data) {
		let toggleHtml = '';

		if (
			typeof $("#component-videos").attr('data-toggle-sports') != 'undefined' || 
			typeof $("#component-videos").attr('data-toggle-schools') != 'undefined'
		) {
			toggleHtml += '<div class="section-header-links">';
			
			if (typeof $("#component-videos").attr('data-toggle-schools') != 'undefined') {
				toggleHtml += '<div class="section-header-link dropdown"> \
						<select id="all-access-school-select"> \
							<option value="0">Select a School...</option> \
							<option value="Boston College">Boston College</option> \
							<option value="Clemson">Clemson</option> \
							<option value="Duke">Duke</option> \
							<option value="Florida State">Florida State</option> \
							<option value="Georgia Tech">Georgia Tech</option> \
							<option value="Louisville">Louisville</option> \
							<option value="Miami">Miami</option> \
							<option value="NC State">NC State</option> \
							<option value="North Carolina">North Carolina</option> \
							<option value="Notre Dame">Notre Dame</option> \
							<option value="Pitt">Pitt</option> \
							<option value="Syracuse">Syracuse</option> \
							<option value="Virginia">Virginia</option> \
							<option value="Virginia Tech">Virginia Tech</option> \
							<option value="Wake Forest">Wake Forest</option> \
						</select> \
					</div>';
			}

			if (typeof $("#component-videos").attr('data-toggle-sports') != 'undefined') {
				toggleHtml += '<div class="section-header-link dropdown"> \
						<select id="all-access-sport-select"> \
							<option value="0">Select a Sport...</option> \
							<option value="Football">Football</option> \
							<option value="Men\'s Basketball">Men\'s Basketball</option> \
							<option value="Women\'s Basketball">Women\'s Basketball</option> \
						</select> \
					</div>';
			}
			
			toggleHtml += '</div>';
		}

		let html = 	'<div class="row"> \
			<div class="small-12 columns"> \
				<div class="section-header clearfix"> \
					<div class="section-header-title"> \
						Videos \
					</div> \
					'+toggleHtml+' \
				</div> \
			</div> \
		</div> \
	  <div class="row"> \
	    <div class="small-12 columns"> \
				<div class="feed"> \
		      <div class="feed-list"></div> \
		      <div class="feed-load-more" data-query=""> \
						<div> \
							<span>Load More Videos</span> \
						</div> \
					</div> \
				</div> \
	    </div> \
	  </div>';

		return html;
	}

	function processVideoFeed(data) {
		let query = JSON.parse($("#component-videos .feed-load-more").attr('data-query'));
		let html = '<div class="row">';

		if(data.length < query.limit) {
			$('#component-videos .feed-load-more').addClass('feed-no-more');
			$('#component-videos .feed-load-more div span').text('No More Videos');
		} else {
			$('#component-videos .feed-load-more').removeClass('feed-no-more');
			$('#component-videos .feed-load-more div span').text('Load More Videos');
		}

		$.each(data, function(index, content) {
			//var date = moment.unix(content.published_at);
			let date = moment(content.created_at).format("ddd, DD MMM YYYY");
			let curDate = moment().format('YYYY-MM-DD');

			if(Object.size(content.images) > 0) {
				imageSrc = content.images.poster.sources[0].src;
			} else {
				imageSrc = 'https://s3.amazonaws.com/sidearm.sites/acc.sidearmsports.com/images/responsive_2019/svg/logo_main.svg';
			}

			html += '<div class="small-12 medium-6 x-large-4 columns"> \
					<div id="'+content.id+'" class="feed-item clearfix">\
						<div class="small-12 large-6 columns"> \
							<div class="responsive-embed"> \
								<img src="'+imageSrc+'" /> \
								<div class="responsive-embed-overlay"> \
									<div></div> \
								</div> \
							</div> \
						</div> \
						<div class="small-12 large-6 columns"> \
							<div class="feed-details"> \
								<div class="feed-details-title">'+content.name+'</div> \
								<div class="feed-details-date">'+date+'</div> \
							</div>\
						</div> \
					</div> \
				</div>';
		}); //end foreach
	
		html += '</div>';

		return html;
	}

	function buildSAQuery(saQuery) {
		let query = '';

		query = typeof saQuery.count != 'undefined' ? query + '&count=' + saQuery.count : query + '&count=6';
		query = typeof saQuery.page != 'undefined' ? query + '&page=' + saQuery.page : query + '&page=1';
		query = typeof saQuery.path != 'undefined' ? query + '&path=' + saQuery.path : query + '&path=';

		return query;
	}

	function componentStories(data) {
		let html = 	'<div class="row"> \
			<div class="small-12 columns"> \
				<div class="section-header clearfix"> \
					<div class="section-header-title"> \
						Related Stories \
					</div> \
				</div> \
			</div> \
		</div> \
	  <div class="row"> \
	    <div class="small-12 columns"> \
				<div class="feed"> \
		      <div class="feed-list"></div> \
		      <div class="feed-load-more" data-query=""> \
						<div> \
							<span>Load More Stories</span> \
						</div> \
					</div> \
				</div> \
	    </div> \
	  </div>';

		return html;
	}

	function processStories(data) {
		let query = JSON.parse($("#component-stories .feed-load-more").attr('data-query'));
		let html = '<div class="row">';

		if(data.length < query.limit) {
			$('#component-stories .feed-load-more').addClass('feed-no-more');
			$('#component-stories .feed-load-more div span').text('No More Stories');
		} else {
			$('#component-stories .feed-load-more').removeClass('feed-no-more');
			$('#component-stories .feed-load-more div span').text('Load More Stories');
		}
		
		data.forEach(function(content, index) {
			var date = content.pubDate;
			var substrEnd = date.length - 12;
			date = date.substr(0, substrEnd);

			html += '<div class="small-12 medium-6 x-large-4 columns"> \
					<a id="'+content.id+'" class="feed-item clearfix" href="'+content.link+'">\
						<div class="small-12 large-6 columns"> \
							<div class="responsive-embed"> \
								<img src="'+content.enclosure.url+'" /> \
							</div> \
						</div> \
						<div class="small-12 large-6 columns"> \
							<div class="feed-details"> \
								<div class="feed-details-title">'+content.title+'</div> \
								<div class="feed-details-date">'+date+'</div> \
							</div>\
						</div> \
					</a> \
				</div>';

		});
		
		html += '</div>';

		return html;
	}

	function processPromos(data) {
		let html = '<div class="row"><div class="small-12 columns"><div class="slick">';

		if(typeof data != 'undefined' && data.length > 0) {
			data.forEach(function(content, index) {
				let target = typeof content.target == 'undefined' ? '_blank' : content.target;
				let linkUrl = typeof content.linkUrl == 'undefined' ? '#' : content.linkUrl;
				let imageAltText = typeof content.imageAltText == 'undefined' ? 'Promo' : content.imageAltText;
				let imageUrl = typeof content.imageUrl == 'undefined' ? 'https://s3.amazonaws.com/sidearm.sites/acc.sidearmsports.com/images/responsive_2019/svg/logo_main.svg' : content.imageUrl;

				html += '<a class="slide" target="'+target+'" href="'+linkUrl+'"> \
						<img alt="'+imageAltText+'" src="'+imageUrl+'" /> \
					</a>';

			});
		} else {
				html += '<div class="slide">No promos found...</div>';
		}

		html += '</div></div></div>';

		return html;
	}

	function processHero(embed, title, date, link) {
    let html = '<a title="'+title+'" class="d-block clearfix" href="'+link+'"> \
      <div class="responsive-embed"> \
				'+embed+' \
			</div> \
	    <div id="hero-details"> \
				<div id="hero-title">'+title+'</div> \
				<div id="hero-date">'+date+'</div> \
      </div> \
    </a>';

		return html;
	}

	function processFeaturedLinks(data) {
		let html = '<h4>Featured Links</h4><ul>';

		if(typeof data != 'undefined' && data.length > 0) {
			data.forEach(function(content, index) {
				html += '<li> \
						<a target="'+content.target+'" href="'+content.url+'">'+content.title+'</a> \
					</li>';
			});
		} else {
			html += '<li> \
					No featured links found... \
				</li>';
		}

		html += '</ul>';

		return html;
	}

	function processSocialTwitter(data) {

		let html = '<div class="section-header clearfix"> \
				<h2 class="section-header-title"> \
					Social \
				</h2> \
				<div class="section-header-links"> \
					<a class="section-header-link" target="_blank" href="https://twitter.com/'+data+'" target="_blank"><i class="sf-twitter"></i> @'+data+'</a> \
				</div> \
			</div> \
      <div class="social-feed">';

		if(typeof data != 'undefined' && data.length > 0) {
			html += '<div class="social-container"> \
					<a class="twitter-timeline" data-chrome="noheader nofooter" data-tweet-limit="10" height="900" href="https://twitter.com/'+data+'" width="100%">Tweets by '+data+'</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script> \
				</div>';
				
		} else {
			html += '<div>No tweets found...</div>';
		}

		html += '</div>';

		return html;
	}

	function processWeather(data) {
		let html = 	'<div class="row"> \
				<div class="small-12 columns"> \
					<div class="section-header clearfix"> \
						<div class="section-header-title"> \
							Local Weather \
						</div> \
					</div> \
				</div> \
			</div> \
			<div class="row"> \
				<div class="small-12 columns"> \
		      <div class="weather-holder"> \
		        <div class="weather-mobile" id="cont_'+data.mobile+'"> \
		          <script type="text/javascript" async src="https://www.theweather.com/wid_loader/'+data.mobile+'"></script> \
		        </div> \
		        <div class="weather-desktop" id="cont_'+data.desktop+'"> \
		          <script type="text/javascript" async src="https://www.theweather.com/wid_loader/'+data.desktop+'"></script> \
		        </div> \
					</div> \
				</div> \
	    </div>';
		return html;
	}

	function processChampionshp(data) {
		processChampionshipHeader(data);	
		if(content.championship.bracket instanceof Object && typeof content.championship.bracket.url != 'undefined') {
		console.log(content.championship.bracket);
			processChampionshipBracket(content.championship.bracket);
		}

		if(typeof content.championship.sa.description != 'undefined') {
			let title;
			if(typeof content.championship.customHtml1Title != 'undefined') {
				title = content.championship.customHtml1Title;
			}
			let html = processCustomHtml(content.championship.sa.description, title);
			$('#component-custom-html-1').append(html);
		}

		if(content.championship.sa.noGamesHtml) {
			let title;
			if(typeof content.championship.customHtml2Title != 'undefined') {
				title = content.championship.customHtml2Title;
			}
			let html = processCustomHtml(content.championship.sa.noGamesHtml, title);
			$('#component-custom-html-2').append(html);
		}

		if(content.championship.sa.extendedHtml) {
			let title;
			if(typeof content.championship.customHtml3Title != 'undefined') {
				title = content.championship.customHtml3Title;
			}
			let html = processCustomHtml(content.championship.sa.extendedHtml, title);
			$('#component-custom-html-3').append(html);
		}
	}

	function processChampionshipHeader(data) {
		let title = '<h1>ACC Tournament</h1>';
		if(typeof data.championship.title != 'undefined') {
			title = '<h1>'+data.championship.title+'</h1>';
		} else if(typeof data.championship.sa.title != 'undefined') {
			title = '<h1>'+data.championship.sa.title+'</h1>';
		}
		let htmlSubtitles = '';
		$.each(data.championship.subtitles, function(index, content) {
			htmlSubtitles = htmlSubtitles + '<h2>'+content+'</h2>';
		});
		let logoURI = typeof data.championship.sa.tournamentLogo.logo != 'undefined' ? data.championship.sa.tournamentLogo.logo.substring(data.championship.sa.tournamentLogo.logo.indexOf('/images')) : 'https://s3.amazonaws.com/sidearm.sites/acc.sidearmsports.com/images/responsive_2019/svg/logo_main.svg';
		let logoAltText = typeof data.championship.sa.tournamentLogo.caption != 'undefined' ? data.championship.sa.tournamentLogo.caption : 'Logo';
		let teaserUrl = typeof data.championship.teaserUrl != 'undefined' ? data.championship.teaserUrl : '';
		let anchorNav = '';

		if(data.championship.headerImages) {
			let htmlStyle = '<style> \
					#component-page-header { \
						background-image: url("'+data.championship.headerImages.mobile+'") !important; \
					} \
					@media (min-width: 768px) { \
						#component-page-header { \
							background-image: url("'+data.championship.headerImages.desktop+'") !important; \
						} \
					} \
				</style>';
			$(htmlStyle).insertBefore('#component-page-header');	
		}

		if(data.videos) {
			anchorNav = anchorNav + '<li><span data-location="component-videos"><i class="sf-video"></i>Videos</span></li>';
		}
		if(data.championship.bracket instanceof Object && typeof data.championship.bracket.url != 'undefined') {
			anchorNav = anchorNav + '<li><span data-location="component-bracket"><i class="sf-list"></i>Bracket</span></li>';
		}	
		if(typeof data.championship.sa.description != 'undefined' && typeof data.championship.customHtml1Title != 'undefined') {
			anchorNav = anchorNav + '<li><span data-location="component-custom-html-1"><i class="sf-info-circle"></i>'+data.championship.customHtml1Title+'</span></li>';
		}
		if(typeof data.championship.sa.noGamesHtml != 'undefined' && typeof data.championship.customHtml2Title != 'undefined') {
			anchorNav = anchorNav + '<li><span data-location="component-custom-html-2"><i class="sf-info-circle"></i>'+data.championship.customHtml2Title+'</span></li>';
		}
		if(data.social instanceof Object && typeof data.social.twitter != 'undefined') {
			anchorNav = anchorNav + '<li><span data-location="component-social-twitter"><i class="sf-share"></i>Social</span></li>';
		}
		if($('#component-weather').length && typeof content.weather != 'undefined') {
			anchorNav = anchorNav + '<li><span data-location="component-weather"><i class="icon-weather-sun"></i>Weather</span></li>';
		}
		if(data.stories) {
			anchorNav = anchorNav + '<li><span data-location="component-stories"><i class="sf-newspaper"></i>Related Stories</span></li>';
		}
		if(typeof data.championship.sa.extendedHtml != 'undefined' && typeof data.championship.customHtml3Title != 'undefined') {
			anchorNav = anchorNav + '<li><span data-location="component-custom-html-3"><i class="sf-info-circle"></i>'+data.championship.customHtml3Title+'</span></li>';
		}
		if($('#component-standings').length && typeof content.championship != 'undefined') {
			anchorNav = anchorNav + '<li><span data-location="component-standings"><i class="sf-list"></i>Standings</span></li>';
		}
		if(typeof data.championship.ticketsUrl != 'undefined') {
			anchorNav = anchorNav + '<li><a target="_blank" href="'+data.championship.ticketsUrl+'"><i class="sf-tickets"></i>Tickets</a></li>';
		} 

		let html = '<div class="row"> \
	    <div class="small-12 columns"> \
	      '+title+' \
				'+htmlSubtitles+' \
	    </div> \
	  </div> \
	  <div class="row logo-video-link">';

		if(anchorNav.length > 0 && typeof data.championship.teaserUrl != 'undefined') {
			anchorNav = 
		  html += 
		    	'<div class="medium-12 large-4 columns"> \
		     		<div id="page-logo"><img "Tournament Logo" alt="'+logoAltText+'" src="'+logoURI+'" /></div> \
		    	</div> \
					<div class="medium-12 large-8 x-large-6 columns"> \
			      <div class="responsive-embed"> \
			        <iframe allowfullscreen="" frameborder="0" scrolling="no" mozallowfullscreen="" webkitallowfullscreen="" src="'+teaserUrl+'"></iframe> \
			      </div> \
			    </div> \
			  </div> \
			  <div class="row"> \
			    <div class="small-12 columns"> \
						<div class="anchor-nav"><ul>'+anchorNav+'</ul></div> \
			    </div> \
			  </div>';
		} else if(anchorNav.length > 0) {
		  html +=
			    '<div class="medium-12 large-5 columns"> \
			      <div id="page-logo"><img "Tournament Logo" alt="'+logoAltText+'" src="'+logoURI+'" /></div> \
			    </div> \
					<div class="medium-12 large-6 x-large-5 columns"> \
						<div class="anchor-nav stacked"><ul>'+anchorNav+'</ul></div> \
			    </div> \
			  </div>';
		} else {
		  html +=
			    '<div class="small-12 columns"> \
			      <div id="page-logo"><img "Tournament Logo" alt="'+logoAltText+'" src="'+logoURI+'" /></div> \
			    </div> \
			  </div>';
		}

		$('#component-page-header').append(html);
		

		$('#anchor-nav ul li span, .anchor-nav ul li span').click(function() {
			var target = $(this).attr('data-location');
			$('html,body').animate({
				scrollTop: $('#'+target).offset().top - scrollToOffset
			}, 250);
			return false;
		});
	}

	function processChampionshipBracket(data) {
		let html = '<div class="section-header clearfix"> \
				<h2 class="section-header-title"> \
					Bracket \
				</h2> \
			</div> \
			<div id="bracket" class="responsive-embed"> \
				<object type="application/pdf" data="https://s3.amazonaws.com/sidearm.sites/acc.sidearmsports.com'+data.url+'?#view=FitH@toolbar=1"> \
					  <p>Your web browser does not have a PDF plugin. Instead you can <a href="https://s3.amazonaws.com/sidearm.sites/acc.sidearmsports.com'+data.url+'">click here to download the PDF file.</a></p> \
				</object> \
			</div>';

			if(typeof data.paragraphs != 'undefined' && typeof data.paragraphs != 'undefined') {
				data.paragraphs.forEach(function(content, index) {
					html += '<p class="muted">'+content+'</p>';
				});
			}

		$('#component-bracket').append(html);
	}

	function processStandings(data) {
		let html = '<div class="row"> \
			<div class="small-12 columns"> \
				<div class="section-header clearfix"> \
					<div class="section-header-title"> \
						'+data.Details.standings_display_title+' \
					</div> \
				</div> \
			</div> \
		</div> \
		<div class="row">';

		for(let i = 0; i < data.Divisions.length; i++) {
			html += processStandingsDivision(data, i);	
		}

		html += '</div>';

		return html;
	}

	function processStandingsDivision(data, division) {
		let html = '';
		if(data.Divisions.length == 2) {
			html += '<div class="small-12 medium-6 columns">'; 
		} else {
			html += '<div class="small-12 columns">'; 
		}
			
		html += '<div class="standings-holder"><table class="sidearm-table">';
		
		if(data.Divisions[division].Title.length) {
			html += '<caption>'+data.Divisions[division].Title+'</caption>';
		}
			
		html += '<thead> \
				<tr> \
					<th>'+data.Standings.FieldNames[0]+'</th> \
					<th>'+data.Standings.FieldNames[1]+'</th> \
					<th>'+data.Standings.FieldNames[2]+'</th> \
				</tr> \
			</thead> \
			<tbody>';

			data.Divisions[division].Schedules.forEach(function(content, index) {
				let needle = content;
				let count = 1;
				data.Standings.Schedules.forEach(function(content, index) {
					if(content.Id == needle) {
            /*
						if(data.Details.sport_id == 3 && content.FieldValues[0] == 'Notre Dame') {
							return;
            }
            */
						html += '<tr> \
							<td>'+content.FieldValues[0]+'</td> \
							<td>'+content.FieldValues[1]+'</td> \
							<td>'+content.FieldValues[2]+'</td> \
						</tr>';
					}
					count++;
				})
			});

		html +=	'</tbody></table></div></div>';

		return html;
	}

	function processCustomHtml(content, title) {
		let html = '';
		if(title) {
			html += '<div class="row"> \
				<div class="small-12 columns"> \
					<div class="section-header clearfix"> \
						<div class="section-header-title"> \
							'+title+' \
						</div> \
					</div> \
				</div> \
			</div>';
		}

		html += '<div class="row"> \
			<div class="small-12 columns"> \
				'+content+' \
			</div> \
		</div>';

		return html;
	}

	Object.size = function(obj) {
	  var size = 0, key;
	  for (key in obj) {
	    if (obj.hasOwnProperty(key)) size++;
	  }
	  return size;
	};

	var isObject = function (obj) {
		return Object.prototype.toString.call(obj) === '[object Object]';
	};

	function addObjectElement (object, element) {
		let newObject = Object.assign(object, element);
		return newObject;
	}

	function equalHeight(container){

		let currentTallest = 0,
				currentRowStart = 0,
				rowDivs = new Array(),
				$el,
				topPosition = 0;

		$(container).each(function() {
			$el = $(this);
			$($el).height('auto')
			topPostion = $el.position().top;

			if (currentRowStart != topPostion) {
				for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
					rowDivs[currentDiv].height(currentTallest);
				}
				rowDivs.length = 0; // empty the array
				currentRowStart = topPostion;
				currentTallest = $el.height();
				rowDivs.push($el);
			} else {
				rowDivs.push($el);
				currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
			}
			for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
				rowDivs[currentDiv].height(currentTallest);
			}
		});
	};

});