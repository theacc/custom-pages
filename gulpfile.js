// npm i gulp gulp-sass autoprefixer cssnano gulp-postcss --save-dev

// Sass configuration
const gulp = require('gulp')
const sass = require('gulp-sass')
const autoprefixer = require('autoprefixer')
const cssnano = require('cssnano')
const postcss = require('gulp-postcss')

/**
 * CSS
 */
function css (done) {
  gulp.src('*.scss')
    .pipe(sass())
    .pipe(postcss([autoprefixer('last 2 version'), cssnano()]))
    .pipe(gulp.dest('.'))
    // .pipe(browsersync.stream())
  done()
}

/**
 * Default Tasks
 */
gulp.task('default', gulp.series(
  gulp.parallel(css), 
  // gulp.watch('*.scss', gulp.series(css))
))