$(window).load(function () {

		var  rsNow = Math.floor(new Date().getTime()/1000.0);

		var scrollToOffset = $('#schoolLogos').height() + $('#navigation').height(),
			winWidth = $(window).width();

		var qellowDeviceName = 'ACC_web_Site',
				qellowDeviceId = '1504271760588.1797431',
				qellowAppVersion = '1';

		if(typeof qTags != 'undefined') {
			var qellowTags = qTags.split(',');
			qTags = '';
			for(var i = 0; i < qellowTags.length; i++) {
			   qTags = qTags + '&tag[]='+qellowTags[i];
			}
		}

		if(typeof qExcludeTags != 'undefined') {
			var qellowExcludeTags = qExcludeTags.split(',');
			qExcludeTags = '';
			for(var i = 0; i < qellowExcludeTags.length; i++) {
			   qExcludeTags = qExcludeTags + '&exclude_tag[]='+qellowTags[i];
			}
		}

		if(qLiveTags == '' || qLiveTags == 'undefined') {
			qLiveTags = '216';
		} else {
			if(qLiveTags.indexOf('216')) {
				qLiveTags = qLiveTags + ',216';
			}
		}

		var qellowLiveTags = qLiveTags.split(',');
		qLiveTags = '';
		for(var i = 0; i < qellowLiveTags.length; i++) {
		   qLiveTags = qLiveTags + '&tag[]='+qellowLiveTags[i];
		}

		var qellow = {
		  token: function() {
		    return $.getJSON('https://api.watchstadium.com/token/?device_data[device_name]='+qellowDeviceName+'&device_data[device_id]='+qellowDeviceId+'&device_data[app_version]='+qellowAppVersion).then(function(data) {
		      return data.data.token;
		    });
		  },
		  video: function(id) {
		    return this.token().then(function(token) {
		    	if(id !== undefined) {
				    return $.getJSON('https://api.watchstadium.com/content/?token='+token+'&id='+id).then(function(data) {
				      return data.data.assets;
				    });
			  	} else {
				    return $.getJSON('https://api.watchstadium.com/content/?token='+token+qTags+'&limit=1&tag_search_type=and&order=created&order_type=desc').then(function(data) {
				      return data.data.assets;
				    });
			  	}
		    });
		  },
		  videos: function(offset) {
		    return this.token().then(function(token) {
					return $.getJSON('https://api.watchstadium.com/content/?token='+token+qTags+'&limit=4&tag_search_type=and&order=created&order_type=desc&offset='+offset).then(function(data) {
			      return data.data.assets;
			    });
		    });
		  },
		  livevideo: function(offset) {
		    	return this.token().then(function(token) {
						//return $.getJSON('https://api.watchstadium.com/liveevents/?token='+token+qLiveTags+'&limit=1&is_live=1&tag_search_type=and&order=created&order_type=asc').then(function(data) {
						return $.getJSON('https://api.watchstadium.com/liveevents/?token='+token+qLiveTags+'&limit=1&is_live=1&tag_search_type=and&order=meta.start_time&meta.end_time_gte_date='+rsNow+'&order_type=asc').then(function(data) {
				      return data.data;
				    });
		    });
		  }
		};

		qellow.livevideo().done(function(data) {
			processLiveVideo(data);
			qellow.videos().done(function(data) {
				processVideoData(data);
			});
		});

		function processVideoData(data) {
  		if(data.length < 4) {
  			$('#load-more-videos').css('display','none');
  			$('#no-more-videos').css('display','block');
  		} else {
  			$('#load-more-videos').css('display','block');
  			$('#no-more-videos').css('display','none');
  		}
			var offset = $("#load-more-videos").attr('data-offset');
			var html = '<div class="row">';
			$.each(data, function(index, content) {
				var date = moment.unix(content.created);
				date = moment(date).format("ddd, DD MMM YYYY");
				var curDate = moment().format('YYYY-MM-DD');
				if(index === 0 && offset == '0') {
				  var $src = 'https://player.watchstadium.com/60/iframe.html?asset_id='+content.id+'&player_id=60&type=vod';
				  $("#player").attr("src", $src);
				  $('#player-title').append(content.meta.name);
				  $('#player-date').append(date);
				}
				html = html + '<div class="medium-12 large-3 columns"> \
						<div id="'+content.id+'" class="feed-item clearfix">\
							<div class="feed-thumb"> \
								<div style="position:relative;"> \
									<img style="display:block;width:100%;height:auto;" src="'+content.images.image+'" /> \
									<div style="position:absolute;top:0;right:0;bottom:0;left:0;height:100%;width:100%;"> \
										<div style="height:60px;width:60px;position:absolute;top:50%;left:50%;transform:translate(-50%, -50%);-ms-transform:translate(-50%, -50%);background:url(http://www.theacc.com/images/video_play_btn_main.png) no-repeat center center;background-size:contain;opacity:0.8;"></div> \
									</div> \
								</div> \
							</div> \
							<div class="feed-details"> \
								<div class="feed-details-title">'+content.meta.name+'</div> \
								<div class="feed-details-date">'+date+'</div> \
							</div>\
						</div> \
					</div>';
			});
			html = html + '</div>';

			$('#playlist').append(html);

			var checkExist = setInterval(function() {
				if ($('#playlist').length) {
					$("#playlist .feed-item").click(function(e) {
					  var $videoId = $(this).attr('id');
						qellow.video($videoId).done(function(data) {
							var $src = 'https://player.watchstadium.com/60/iframe.html?asset_id='+$videoId+'&player_id=60&type=vod';
							var date = moment.unix(data[0].created);
							date = moment(date).format("MMM., Do YYYY");

						  $("#player").attr("src", $src);
						  $('#player-title').html(data[0].meta.name);
						  $('#player-date').html(date);
			        $('html,body').animate({
			          scrollTop: $('#player-holder').offset().top - scrollToOffset
			        }, 250);
						});
					})
			    clearInterval(checkExist);
			  }
			}, 100);
		}

		$('#load-more-videos').click(function(){
			var offset = $("#load-more-videos").attr('data-offset');
			offset = parseInt(offset) + 4;
			$("#load-more-videos").attr('data-offset', offset);
			qellow.videos(offset).done(function(data) {
				processVideoData(data)
			});
		});


		function refreshLiveVideo() {
			qellow.livevideo().done(function(data) {
				if(saPath == 'mbball') {
					var str = $('#page-header-video-iframe').attr('src');
				} else {
					var str = $('#player').attr('src');
				}
				if(data.total > 0 && str.indexOf('liveevent') == -1) {
					processLiveVideo(data);
				}
				setTimeout(refreshLiveVideo, 5000);
			});
		}

		// initial call, or just call refresh directly
		//setTimeout(refreshLiveVideo, 5000);
		refreshLiveVideo();

		function processLiveVideo(data) {
			$.each(data.liveevents, function(index, content) {
				var date = moment.unix(content.meta.start_time);
				date = moment(date).format("ddd, DD MMM YYYY");
				if(index === 0) {
					if(saPath == 'mbball') {
					  var $src = 'https://player.watchstadium.com/60/iframe.html?asset_id='+content.id+'&player_id=60&type=liveevent';
					  $("#page-header-video-iframe").attr("src", $src);
					} else {
					  var $src = 'https://player.watchstadium.com/60/iframe.html?asset_id='+content.id+'&player_id=60&type=liveevent';
					  $("#player").attr("src", $src);
					  $('#player-title').html(content.meta.name);
					  $('#player-date').html(date);
					}
				}
			});
		}

	var sidearm = {
		stories : function(page) {
			if(page === undefined) {
				var page = $("#load-more-stories").attr('data-page');
			} else {
				var page = page;
			}
			return $.getJSON('/api/stories_xml?format='+saFormat+'&path='+saPath+'&count=4&page='+page).then(function(data) {
				return data.stories;
			});
		},
		standings: function() {
			return $.getJSON('http://theacc.com/api/standings?view='+saView+'&detail='+saDetail+'&path='+saPath).then(function(data) {
				return data;
			});
		}
	};

	sidearm.stories().done(function(data) {
		processHeros(data);
	});


	$('#load-more-stories').click(function(){
		var page = $("#load-more-stories").attr('data-page');
		page = parseInt(page) + 1;
		$("#load-more-stories").attr('data-page', page);
		sidearm.stories(page).done(function(data) {
			processHeros(data)
		});
	});

	function processHeros(data) {
		var page = $("#load-more-stories").attr('data-page');
		var html = '<div class="row">';
		$.each(data, function(index, content) {
			var date = content.pubDate;
			var substrEnd = date.length - 12;
			date = date.substr(0, substrEnd);
			if(index === 0 && page == '1') {
				$("#story-container").attr("href", content.link);
				$("#story").attr("src", content.enclosure.url);
				$('#story-title').append(content.title);
				$('#story-date').append(date);
			}
			html = html + '<div class="medium-12 large-3 columns"> \
			<a class="feed-item" href="'+content.link+'">\
				<div class="feed-thumb" style="background-image:url('+content.enclosure.url+');"></div> \
				<div class="feed-details"> \
					<div class="feed-details-title">'+content.title+'</div> \
					<div class="feed-details-date">'+date+'</div> \
				</div>\
			</a> \
		</div>';
	});
		html = html + '</div>';

		$('#stories').append(html);
	}

	$('#anchor-nav ul li span, .anchor-nav ul li span').click(function() {
		var target = $(this).attr('data-location');
		$('html,body').animate({
			scrollTop: $('#'+target).offset().top - scrollToOffset
		}, 1500);
		return false;
	});

		/*
		var checkLiveExist = setInterval(function() {
			qellow.livevideo().done(function(data) {
				if(data.total > 0) {
					processLiveVideo(data);
					clearInterval(checkLiveExist);
				}
			});
		}, 5000);
		*/
/*
		(function() {
		    function checkLiveExist() {
					qellow.livevideo().done(function(data) {
						if(data.total > 0) {
							processLiveVideo(data);
							clearInterval(checkLiveExist);
							return;
						}
					});
		    }
		    checkLiveExist();
		    setInterval(checkLiveExist, 10000);
		})();
*/
		/*
		setInterval(function hello() {
		  console.log('world');
		  return hello;
		}(), 5000);

		*/

		/*
		(function checkLiveExist() {
			qellow.livevideo().done(function(data) {
				if(data.total > 0) {
					processLiveVideo(data);
				} else {
	    		setTimeout(checkLiveExist, 20000);
				}
			});
		})();
		*/

		/*
		qellow.livevideo().done(function(data) {
			if(data.total > 0) {
				processLiveVideo(data);
			}
		});
		*/

});
